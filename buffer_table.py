"""
Graphical, sortable representation of the current buffer usage

Authors: Mitja Mueller-Jend
License: MIT
Installation:
    - Place the BufferReport folder inside your .nuke or PYTHONPATH
    - Add following to your menu.py:

menu = nuke.menu('Nuke').findItem("Cache")
menu.addCommand('Buffer Report', 'from BufferReport import buffer_table;br_widget = buffer_table.BufferReportWidget();br_widget.show()')

    - If you want the BufferReport as dockable widget also add these lines:

from nukescripts import panels

def get_buffer_report_widget():
    # only import if needed and make the widget restorable from saved layout
    from BufferReport import buffer_table
    br_widget = buffer_table.BufferReportWidget()
    return  br_widget

pane = nuke.getPaneFor('Properties.1')
panels.registerWidgetAsPanel('get_buffer_report_widget', 'Buffer Report', 'de.filmkorn.BufferReport', True).addToPane(pane)
"""

from Qt import QtCore, QtWidgets
import xml.etree.ElementTree as ET
import nuke


def get_bufferreport_tree():
    buffer_xml = nuke.memory("infoxml")
    return ET.fromstring(buffer_xml)


def sizeof_fmt(num, suffix='B'):
    unit_list = ['', 'K', 'M', 'G', 'T', 'P']
    for unit in unit_list:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        else:
            num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


class BufferReportTreeWidgetItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, parent, name, memUsage):
        QtWidgets.QTreeWidgetItem.__init__(self, parent, [name, sizeof_fmt(int(memUsage))])

        self.memUsage = memUsage

    def __lt__(self, other):
        """enables comparison for correct sorting
        """
        column = self.treeWidget().sortColumn()

        if column == 1:
            key1 = self.memUsage
            key2 = other.memUsage
            return int(key1) < int(key2)
        else:
            key1 = self.text(column)
            key2 = other.text(column)
            return key1 < key2


class UsageBar(QtWidgets.QProgressBar):
    def __init__(self, parent=None):
        QtWidgets.QProgressBar.__init__(self, parent)
        self._text = None

    def setText(self, text):
        self._text = text

    def text(self):
        return self._text


class BufferReportTreeWidget(QtWidgets.QTreeWidget):
    # TODO: Create abstract Class to display xml created by performance profiling
    # TODO: translate to Model/View programming
    # TODO: implement interactive pie chart

    usage = 0  # as 100 percent for usage bars

    def __init__(self, parent=None):
        QtWidgets.QTreeWidget.__init__(self, parent)
        self.reportTree = None
        self.nodesCount = 0
        self.listItems = ["Name", "MemUsage", "cacheRegion", "channels", "usage", "weight", "allocRegion",
                          "allocHeightInfo"]
        self.setColumnCount(len(self.listItems))
        self.setColumnWidth(0, 200)
        self.setColumnWidth(1, 250)
        self.setColumnWidth(3, 200)
        self.setHeaderLabels(self.listItems)

        # initial sort order: by memory
        self.sortItems(1, QtCore.Qt.DescendingOrder)

        self.itemClicked.connect(self.select_node)

    def update(self):
        # disable sorting for better load perf
        self.setSortingEnabled(False)
        self.usage = nuke.memory("usage")
        self.clear()
        self.reportTree = get_bufferreport_tree()

        # populate
        items = [self.create_item(self, child) for child in self.reportTree.getchildren()]
        self.addTopLevelItems(items)
        if items:
            self.setCurrentItem(items[0])

        # enable sorting
        self.setSortingEnabled(True)

    def create_item(self, parent, root):
        """recursively create items

        if child's tag is "UserData", add the children to the current item and stop recursion.

        Args:
            parent: parent widget
            root: current tree item

        Returns:
            BufferReportTreeWidgetItem: created widget item
        """
        item = BufferReportTreeWidgetItem(parent, root.get('Name'), root.get('MemUsage'))
        if root.get("MemUsage"):
            if root.tag == "Node":
                usage_bar = UsageBar(self)
                usage_bar.setRange(0, 100)
                usage_bar.setValue(100 * int(root.get("MemUsage")) / float(self.usage))
                usage_bar.setText(sizeof_fmt(int(root.get("MemUsage"))))
                self.setItemWidget(item, 1, usage_bar)

        for child in root.getchildren():
            if child.tag == "UserData":
                if child.get('Name') in self.listItems:
                    item.setText(self.listItems.index(child.get('Name')), child.text)
            else:
                self.create_item(item, child)
        return item

    @staticmethod
    def select_node(item, column):

        # deselecting all nodes:  looks stupid but works in non-commercial mode
        nuke.selectAll()
        nuke.invertSelection()
        node_name = item.text(0)

        # if node is part of a group: select the group
        if "." in node_name:
            node_name = node_name.split(".")[0]

        node = nuke.toNode(node_name)
        if node:
            node['selected'].setValue(True)
            nuke.zoom(1, [node.xpos(), node.ypos()])


class BufferReportWidget(QtWidgets.QDialog):
    total_memory = nuke.memory("total_ram")
    usage = 0

    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)

        self.setWindowTitle("Buffer Report")
        self.resize(1200, 550)

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        self.treeWidget = BufferReportTreeWidget()
        self.layout.addWidget(self.treeWidget)

        self.memory_bar = QtWidgets.QProgressBar()
        self.memory_bar.setRange(0, 100)  # not using total_memory here to prevent overflow error
        self.layout.addWidget(self.memory_bar)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.open_menu)

        self.update()

    def update(self):
        self.treeWidget.update()
        self.update_memory_bar()

    def update_memory_bar(self):
        self.usage = nuke.memory("usage")
        self.memory_bar.setValue(100 * self.usage / float(self.total_memory))

    def clear_buffers(self):
        nuke.memory('free')
        self.update()

    def open_menu(self, position):
        menu = QtWidgets.QMenu()

        update_action = QtWidgets.QAction("&Update", self)
        update_action.setShortcut("F5")
        update_action.triggered.connect(self.update)
        menu.addAction(update_action)

        clear_action = QtWidgets.QAction("&Clear Buffers", self)
        clear_action.triggered.connect(self.clear_buffers)
        menu.addAction(clear_action)

        menu.addSeparator()

        close_action = QtWidgets.QAction("&Close", self)
        close_action.setShortcut("Esc")
        close_action.triggered.connect(self.close)
        menu.addAction(close_action)

        selected = menu.exec_(self.mapToGlobal(position))


if __name__ == "__main__":
    brWidget = BufferReportWidget()
    brWidget.show()
